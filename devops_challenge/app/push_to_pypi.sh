# RUN with: docker run -it -v /var/run/docker.sock:/var/run/docker.sock -v /home/liron/my-python3-examples/devops_challenge/app:/app python sh -c "bash /app/push_to_pypi.sh 0.3"
cp -r /app build
mv build/.pypirc ~/
python -m pip install --upgrade pip setuptools wheel
python -m pip install tqdm
python -m pip install --user --upgrade twine

cd build
sed -i "s|{{VERSION}}|$1|g" setup.py
python setup.py bdist_wheel
python -m twine upload dist/*
