# test_hello.py
from hello import app

def test_hello():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert response.data == b'Hello, World!'


def test_greet():
    response = app.test_client().get('/greet')

    assert response.status_code == 200
    assert response.data == b'greetings'

def main():
    try: 
       test_hello()
       test_greet()
       print('success')
    except:
       print('failure')

if __name__ == "__main__":
    main()
